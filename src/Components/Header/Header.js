import react from "react"
import logo from "../../assets/logo_tienda.jpg"
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import "./Header.css"
import Navbar from "../Navbar/Navbar";
import CartWidget from "../CartWidget/cartWidget";



const Header = ({name,app})=>{
    
    
    return (
        <header className="Header">
            <div className="shoppingCar">
                <CartWidget/>
            </div>
            <div>
                <div className="titleLogo">
                    <h1>Camisetas Online</h1>
                </div>   
                <Navbar/>
            </div>
        </header>
    )
        
        
}
    
export default Header